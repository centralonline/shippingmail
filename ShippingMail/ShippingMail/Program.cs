﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using RestSharp;

namespace ShippingMail
{
    class Program
    {
#if DEBUG
        public static readonly string ENV = ConfigurationManager.AppSettings["env_uat"];
        public static readonly string Domain = ConfigurationManager.AppSettings["domain_uat"];
        public static readonly string ConnectionStringData = @"Data Source=10.17.251.160;Initial Catalog=DBCDSData;User ID=central;Password=Cen@tral";

#else
			public static readonly string ENV = ConfigurationManager.AppSettings["env_prd"];
			public static readonly string Domain = ConfigurationManager.AppSettings["domain_prd"];
			public static readonly string ConnectionStringData = @"Data Source=10.17.220.55;Initial Catalog=DBCDSData;User ID=central;Password=Cen@tral";
#endif

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Start Program Send mail shipping");
                Console.WriteLine($"Env : {ENV}");
                Console.WriteLine($"Domain : {Domain}");
                Console.WriteLine($"DateTime run : {DateTime.Now}");
                Console.WriteLine($"Delay for start program 5 second");
                Thread.Sleep(5000);
                List<string> suborderIds = new List<string>();
                using (SqlConnection connect = new SqlConnection(ConnectionStringData))
                {
                    connect.Open();
                    using (SqlCommand command = connect.CreateCommand())
                    {
                        string sql = @"SELECT soh.SubOrderId FROM TBSubOrderHead soh 
	                                    left join TBSubOrderStatusMail m on (soh.SubOrderId = m.SubOrderId)
                                    WHERE soh.Status in ('Shipping','Delivered') and m.SubOrderId is null ";
                        command.CommandText = sql;
                        command.CommandType = CommandType.Text;
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                suborderIds.Add((string)reader["SubOrderId"]);
                            }
                        }
                        SendComplete(suborderIds, command);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception : {e.ToString()}");
            }
            Console.WriteLine($"End Program");
            Thread.Sleep(5000);
        }

        static void SendComplete(List<string> suborderIds, SqlCommand command)
        {
            foreach (var suborderid in suborderIds)
            {
                try
                {
                    Console.WriteLine($"SubOrderId : {suborderid}");
                    Thread.Sleep(10000);
                    var resource = "api/SendMail.aspx";
                    var request = new RestRequest(resource, Method.POST);
                    request.AddParameter("EmailType", 8);
                    request.AddParameter("SubOrderId", suborderid);
                    var response = new RestResponse();
                    RestClient client = new RestClient(Domain);
                    Task.Run(async () =>
                    {
                        response = await GetResponseContentAsync(client, request) as RestResponse;
                    }).Wait();
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        UpdateSendComplete(suborderid, command);
                        Console.WriteLine($"SubOrderId : {suborderid} StatusCode : {System.Net.HttpStatusCode.OK}");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Exception OrderGuId : {e.ToString()}");
                }
            }
        }
        static void UpdateSendComplete(string suborderid, SqlCommand command)
        {
            string query = string.Empty;
            query = @"INSERT INTO TBSubOrderStatusMail(SubOrderId,IsSendMailShipping) VALUES (@SubOrderId,1)";
            command.CommandText = query;
            command.Parameters.AddWithValue("@SubOrderId", suborderid);
            command.CommandType = CommandType.Text;
            command.ExecuteNonQuery();
            command.Parameters.Clear();
        }



        public static RestResponse PostAsync(string resource, string authorization, string message)
        {
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", authorization);
            request.AddQueryParameter("message", message);
            var response = new RestResponse();
            RestClient client = new RestClient(resource);
            Task.Run(async () =>
            {
                response = await GetResponseContentAsync(client, request) as RestResponse;
            }).Wait();
            return response;
        }

        public static Task<IRestResponse> GetResponseContentAsync(RestClient theClient, RestRequest theRequest)
        {
            var tcs = new TaskCompletionSource<IRestResponse>();
            theClient.ExecuteAsync(theRequest, response => {
                tcs.SetResult(response);
            });
            return tcs.Task;
        }
    }
}
